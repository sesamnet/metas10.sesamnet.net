# -----------------------------------
# Generelle Einstellungen
# -----------------------------------

# for debugging
config.contentObjectExceptionHandler = 0

config {
    doctype = html5
    xmlprologue = none
    htmlTag_setParams = lang="de"

    spamProtectEmailAddresses = 1
    spamProtectEmailAddresses_atSubst = (at)

    # Kein Cache!
    # no_cache=1

    # Eintrag in Logfile sicherstellen
    #stat_apache=1
    #stat_apache_logfile = logfile.txt

    # Angaben für RealUrl
    absRefPrefix = /
    tx_realurl_enable = 1

    # Variable für Sprache
    linkVars = L
    sys_language_overlay = 0

    # Standartseitentitel entfernen
    noPageTitle = 2
}

# -----------------------------------
# Deutsch Standardsprache
# -----------------------------------
config {
    sys_language_uid = 0
    language = de
    locale_all = de_DE.UTF8
}

page {
    meta {
        #Sprache im Head definieren (ISO Codes verwenden)
        content-language = de

        description = Eidgenössisches Institut für Metrologie METAS
        # Wenn Description von SEO Basics vorhanden -> überschreiben
        description.override {
            required = 1
            10 = TEXT
            10.data = page:title
            10.htmlSpecialChars = 1
        }

        keywords = Eidgenössisches Institut für Metrologie METAS

        # Wenn Keywords von SEO Basics vorhanden -> überschreiben
        keywords.override {
            required = 1
            data = field:keywords
        }
    }

    # Title Tag definieren
    headerData.5 = TEXT
    headerData.5 {
        data = field:tx_seo_titletag
        wrap = <title>|</title>

        ifEmpty.field = title//subtitle
        ifEmpty.wrap = Eidgenössisches Institut für Metrologie METAS [|]
    }

    headerData.10 = COA
    headerData.10 {
        1 = TEXT
        1.value (
      <link rel="apple-touch-icon" sizes="57x57" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/apple-touch-icon-57x57.png">
      <link rel="apple-touch-icon" sizes="60x60" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/apple-touch-icon-60x60.png">
      <link rel="apple-touch-icon" sizes="72x72" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/apple-touch-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="76x76" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/apple-touch-icon-76x76.png">
      <link rel="apple-touch-icon" sizes="114x114" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/apple-touch-icon-114x114.png">
      <link rel="apple-touch-icon" sizes="120x120" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/apple-touch-icon-120x120.png">
      <link rel="apple-touch-icon" sizes="144x144" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/apple-touch-icon-144x144.png">
      <link rel="apple-touch-icon" sizes="152x152" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/apple-touch-icon-152x152.png">
      <link rel="apple-touch-icon" sizes="180x180" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/apple-touch-icon-180x180.png">
      <link rel="icon" type="image/png" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/favicon-32x32.png" sizes="32x32">
      <link rel="icon" type="image/png" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/android-chrome-192x192.png" sizes="192x192">
      <link rel="icon" type="image/png" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/favicon-96x96.png" sizes="96x96">
      <link rel="icon" type="image/png" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/favicon-16x16.png" sizes="16x16">
      <link rel="manifest" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/manifest.json">
      <link rel="shortcut icon" href="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/favicon.ico">
      <meta name="apple-mobile-web-app-title" content="METAS">
      <meta name="application-name" content="METAS">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/mstile-144x144.png">
      <meta name="msapplication-config" content="/typo3conf/ext/snmetas/Resources/Public/Images/favicons/browserconfig.xml">
      <meta name="theme-color" content="#ffffff">
        )
        3 = TEXT
        3.value (
	<script src="/typo3conf/ext/snmetas/Resources/Public/Scripts/custom.js" type="text/javascript"></script>
	
	<script src="/typo3conf/ext/metaspostit/Resources/Public/css-js/postit.js" type="text/javascript"></script>

        )
    }
}

# -----------------------------------
# Französische Sprache
# -----------------------------------
[globalVar = GP:L = 1]
    config {
        sys_language_uid = 1
        language = fr
        locale_all = fr_FR
    }

    page {
        meta {
            # Sprache im Head definieren (ISO Codes verwenden)
            content-language = fr
            description = Eidgenössisches Institut für Metrologie METAS

            # Wenn Description von SEO Basics vorhanden -> überschreiben
            description.override {
                required = 1
                10 = TEXT
                10.data = page:title
                10.htmlSpecialChars = 1
            }

            keywords = Eidgenössisches Institut für Metrologie METAS

            # Wenn Keywords von SEO Basics vorhanden -> überschreiben
            keywords.override {
                required = 1
                data = field:keywords
            }
        }

        # Title Tag definieren
        headerData.5 = TEXT
        headerData.5 {
            data = field:tx_seo_titletag
            wrap = <title>|</title>
            ifEmpty.field = title//subtitle
            ifEmpty.wrap = Eidgenössisches Institut für Metrologie METAS [|]
        }
    }
[global]

# -----------------------------------------
# NICEDITOR - FRONTEND EDITOR
# -----------------------------------------
[globalVar = TSFE:id = 2]
    page.headerData.10 = COA
    page.headerData.10 {
        2 = TEXT
        2.value (
  <script type="text/javascript">
  
$(function() {
  
  if($(".rteeditor").length) {
    # Anpassung 25.11.2015
    bkLib.onDomLoaded(function() { 
      new nicEditor({buttonList : ['bold','italic','underline', 'ol', 'ul', 'link', 'unlink', 'removeformat', 'forecolor']}).panelInstance('rtearea');
    });
      };
  
});



  </script>
        )
    }
[global]

# -----------------------------------------
# INDEXED SEARCH
# -----------------------------------------
plugin.tx_indexedsearch {
    # Externes Template einbinden
    templateFile = fileadmin/templates/indexed-search-tmpl.html

    # Standardwerte angeben
    _DEFAULT_PI_VARS {
        extResume = 1

        # Defaultmässig ein Teilwort suchen (1=Ein)
        # --------------------------------------
        type = 1
        lang < config.sys_language_uid

        # Erweiterte Suchfunktion bei Suche anzeigen
        # ------------------------------------------
        ext = 0

        # grouping: flat/sections
        # ------------------------------------------
        group = flat

        # Resultate pro Seite anzeigen
        # ------------------------------------------
        results = 10
    }

    # wrappings
    rules_stdWrap {

    }

    sectionlinks_stdWrap {

    }

    path_stdWrap {

    }

    # Konfiguration Anzeige Resultate
    # -------------------------------
    search {
        # Start Suche ab dieser PID Nummer
        # --------------------------------
        rootPidList = 1

        # Anzahl Pagings-Links anzeigen (Seite1 Seite2 Seite3, etc..)
        # -----------------------------------------------------------
        page_links = 4

        detect_sys_domain_records = 0
        #defaultFreeIndexUidList = 0,1,2
    }

    # show
    show {
        # Suchregeln/Beschreibung der Suche anzeigen (0 = Nein)
        # -----------------------------------------------------
        rules = 0

        advancedSearchLink = 0

        # hash creation
        parsetimes = 1

        # second level in section dropdown
        L2sections = 0

        # first level in section dropdown
        L1sections = 1

        # show "not in menu" or "hide from menu" but not hidden pages in section
        LxALLtypes = 0

        # empty formfield after search
        clearSearchBox = 0

        # add searchterm to history
        clearSearchBox.enableSubSearchCheckBox = 1

        forbiddenRecords = 0,72,14,73
        alwaysShowPageLinks = 0
        advancedSearchLink = 0
        resultNumber = 3
        mediaList = 1
    }

    # show fields for parameters
    blind {
        # type (word, subpart of word, ..)
        type = 0

        # default option (and, or)
        defOp = 0

        # sections of website
        sections = 0

        # search in mediatypes
        media = 1

        # sort
        order = 0

        # view (section hierarchye / list)
        group = 0

        # language selection
        lang = 1

        # select sorting
        desc = 0

        # results per page
        results = 5

        # extended preview
        extResume = 0

        #freeIndexUid = 0
    }
}

# -----------------------------------------
# Mainnavigation
# -----------------------------------------
lib.mainNavi = HMENU
lib.mainNavi.entryLevel = 0
lib.mainNavi.1 = TMENU
lib.mainNavi.1 {
    wrap = <ul>|</ul>
    expAll = 0
    NO.allWrap = <li>|</li>
    RO < .NO
    RO = 1
    CUR < .NO
    CUR = 1
    CUR.allWrap = <li class="active">|</li>
    ACT < .CUR
}

# -----------------------------------------
# SUBNAVIGATION
# -----------------------------------------
lib.subNavi = HMENU
lib.subNavi {
    entryLevel = 1
    1 = TMENU
    1 {
        wrap = <ul>|</ul>
        expAll = 0
        NO.wrapItemAndSub = <li>|</li>
        ACT = 1
        ACT.wrapItemAndSub = <li class="active">|</li>
        ACTIFSUB = 1
        ACTIFSUB.wrapItemAndSub = <li class="active sub">|</li>
        IFSUB = 1
        IFSUB.wrapItemAndSub = <li class="havesub">|</li>
    }

    2 = TMENU
    2 {
        wrap = <ul>|</ul>
        NO.wrapItemAndSub = <li>|</li>
        ACT = 1
        ACT.wrapItemAndSub = <li class="active">|</li>
    }
}

# -----------------------------------------
# SPRACHNAVIGATION
# -----------------------------------------
lib.sprachNavi = HMENU
lib.sprachNavi {
    # Ein Sprach-Menü wird erzeugt
    special = language
    # Reihenfolge und Auswahl der Sprachen im Menü
    special.value = 0,1
    special.normalWhenNoLanguage = 0
    wrap = <ul>|</ul>
    1 = TMENU
    1 {
        noBlur = 1
        # Standard Sprachen
        NO = 1
        NO {
            linkWrap = <li>|</li>
            # Standard-Titel für den Link wäre Seitenttitel
            # => anderer Text als Link-Text (Optionsschift)
            stdWrap.override = DE || FR
            # Standardmäßige Verlinkung des Menüs ausschalten
            # Da diese sonstige GET-Parameter nicht enthält
            doNotLinkIt = 1
            # Nun wird der Link mit den aktuellen GET-Parametern neu aufgebaut
            stdWrap.typolink.parameter.data = page:uid
            stdWrap.typolink.additionalParams = &L=0 || &L=1 ||
            stdWrap.typolink.addQueryString = 1
            stdWrap.typolink.addQueryString.exclude = L,id,cHash,no_cache
            stdWrap.typolink.addQueryString.method = GET
            stdWrap.typolink.useCacheHash = 1
            stdWrap.typolink.no_cache = 0
        }

        # Aktive Sprache
        ACT < .NO
        ACT.linkWrap = <li class="active">|</li>
        # NO + Übersetzung nicht vorhanden
        USERDEF1 < .NO
        USERDEF1 {
            stdWrap.typolink >
        }

        USERDEF2 < .ACT
        USERDEF2 {
            stdWrap.typolink >
        }
    }
}

# -----------------------------------------
# SKIPLINKS
# -----------------------------------------
lib.skiplinks = TEXT
lib.skiplinks {
    value (
        <ul class="skiplinks">
            <li> <a href="/" accesskey="0" title="[ALT + 0]">Startseite</a> </li>
            <li> <a href="#menu" accesskey="1" title="[ALT + 1]">Navigation</a> </li>
            <li> <a href="#content" accesskey="2" title="[ALT + 2]">Zum Inhalt</a> </li>
            <li> <a href="/de/kontakte/" accesskey="3" title="[ALT + 3]">Kontakt</a> </li>
            <!--<li> <a href="/de/sitemap/" accesskey="4" title="[ALT + 4]">Sitemap</a> </li>-->
        </ul>
    )
}

[globalVar = GP:L = 1]
    lib.skiplinks = TEXT
    lib.skiplinks {
        value (
        <ul class="skiplinks">
            <li> <a href="/" accesskey="0" title="[ALT + 0]">Accueil</a> </li>
            <li> <a href="#menu" accesskey="1" title="[ALT + 1]">Navigation</a> </li>
            <li> <a href="#content" accesskey="2" title="[ALT + 2]">Contenu</a> </li>
            <li> <a href="/fr/contact/" accesskey="3" title="[ALT + 3]">Contact</a> </li>
            <!--<li> <a href="/de/sitemap/" accesskey="4" title="[ALT + 4]">Sitemap</a> </li>-->
        </ul>
        )
    }
[global]

# -----------------------------------------
# LOGOS AUS BACKEND
# -----------------------------------------
lib.logo = CONTENT
lib.logo {
    table = tt_content
    select.where = colPos = 90
    slide = -1
    select.languageField = sys_language_uid
    stdWrap.typolink.parameter = 2
}

# -----------------------------------------
# TITEL FUER METABOARD
# -----------------------------------------
lib.postitTitel = TEXT
lib.postitTitel.value = <h3>Post it</h3>

lib.interessantesTitel = TEXT
# Anpassung 25.11.2015
lib.interessantesTitel.value = <h3>Transfer</h3>

lib.informationenTitel = TEXT
lib.informationenTitel.value = <h3>Informationen</h3>

[globalVar = GP:L = 1]
    #Titel für Metboard
    lib.postitTitel = TEXT
    lib.postitTitel.value = <h3>Post it</h3>

    lib.interessantesTitel = TEXT
    # Anpassung 25.11.2015
    lib.interessantesTitel.value = <h3>Transfer</h3>

    lib.informationenTitel = TEXT
    lib.informationenTitel.value = <h3>Informations</h3>
[global]

# -----------------------------------------
# HEADERBILD
# -----------------------------------------
lib.headerBild = CONTENT
lib.headerBild {
    table = tt_content
    select.where = colPos = 91
    slide = -1
    select.languageField = sys_language_uid
    #stdWrap.typolink.parameter = 2
}

# -----------------------------------------
# HEUTE AKTUELL
# -----------------------------------------
lib.heuteAktuell = CONTENT
lib.heuteAktuell {
    table = tt_content
    select.where = colPos = 93
    slide = -1
    select.languageField = sys_language_uid
}

[globalVar = TSFE : beUserLogin > 0]
    lib.angemeldet = TEXT
    lib.angemeldet {
        value (
    1
        )
    }
[global]

# -----------------------------------------
# FOOTER TEXT
# -----------------------------------------
lib.footerText = CONTENT
lib.footerText {
    table = tt_content
    select.where = colPos = 92
    slide = -1
    select.languageField = sys_language_uid
}

# -----------------------------------------
# FOOTER NAVIGATION
# -----------------------------------------
lib.footerNavi = HMENU
lib.footerNavi {
    wrap = <ul>|</ul>
    special = directory
    special.value = 12
    1 = TMENU
    1.NO = 1
    1.NO.linkWrap = <li>|</li>
}

# ------------------------------------
# Indexed Search Vorlage START
# ------------------------------------
# Suchmaschine anwerfen :)
page.config.index_enable = 1
page.config.index_externals = 1

# ------------------------------------
#Suchformular für jede Seite
#----------------------------
# DE
lib.suchFeld = TEXT
lib.suchFeld.value (
  <form action='/de/suchformular/' id='suchformular' method='post'>
    <label class="visuallyhidden" for="suchfeld">Suchfeld</label>
    <input class='suchfeld' id='suchfeld' type='text' name='tx_indexedsearch[sword]' placeholder="Suche">
    <label class="visuallyhidden" for="suchbutton">Suchbutton</label>
    <input class='suche-btn absenden' id="suchbutton" name="suchbutton" type='submit' value='Suchbutton' />
  </form>
)

#FR
[globalVar = GP:L = 1]
    lib.suchFeld = TEXT
    lib.suchFeld.value (
  <form action='/fr/recherche/' id='suchformular' method='post'>
  	<label class="visuallyhidden" for="tx_indexedsearch[sword]">champ recherche</label>
    <input class='suchfeld' id='suchfeld' type='text' name='tx_indexedsearch[sword]'>
    <label class="visuallyhidden" for="suchbutton">bouton de recherche</label>
    <input class='suche-btn absenden' name="suchbutton" type='submit' value='bouton de recherche' placeholder="Recherche" />
  </form>
    )
[global]


lib.suchlink = TEXT
lib.suchlink.value = /de/suchformular/

[siteLanguage("languageId") == "1"]
    lib.suchlink.value = /fr/suchformular/
[END]

# ------------------------------------
# VISUALLYHIDDEN
# ------------------------------------
#Header
lib.visuallyHauptnavigation = TEXT
lib.visuallyHauptnavigation.value = Hauptnavigation
lib.visuallySuche = TEXT
lib.visuallySuche.value = Suche
lib.visuallySprache = TEXT
lib.visuallySprache.value = Sprachauswahl

#Postit / Interesantes / Informationen
lib.newTitle = TEXT
lib.newTitle.value = Neu
lib.editTitle = TEXT
lib.editTitle.value = Bearbeiten

#Interessantes
lib.visuallyNewinteresse = TEXT
lib.visuallyNewinteresse.value = Neu
lib.editInteresse = TEXT
lib.editInteresse.value = Bearbeiten

#Postit
lib.visuallyNewpostit = TEXT
lib.visuallyNewpostit.value = Neu
lib.editPostit = TEXT
lib.editPostit.value = Bearbeiten

#Informationen
lib.visuallyNewinformationen = TEXT
lib.visuallyNewinformationen.value = Neu
lib.editInformationen = TEXT
lib.editInformationen.value = Bearbeiten

[globalVar = GP:L = 1]
    #Header
    lib.visuallyHauptnavigation = TEXT
    lib.visuallyHauptnavigation.value = navigation principale
    lib.visuallySuche = TEXT
    lib.visuallySuche.value = rechercher
    lib.visuallySprache = TEXT
    lib.visuallySprache.value = sélection de la langue

    #Postit / Interesantes / Informationen
    lib.newTitle = TEXT
    lib.newTitle.value = Nouveau
    lib.editTitle = TEXT
    lib.editTitle.value = Editer

    #Interessantes
    lib.visuallyNewinteresse = TEXT
    lib.visuallyNewinteresse.value = Nouveau
    lib.editInteresse = TEXT
    lib.editInteresse.value = Editer

    #Postit
    lib.visuallyNewpostit = TEXT
    lib.visuallyNewpostit.value = Nouveau
    lib.editPostit = TEXT
    lib.editPostit.value = Editer

    #Informationen
    lib.visuallyNewinformationen = TEXT
    lib.visuallyNewinformationen.value = Nouveau
    lib.editInformationen = TEXT
    lib.editInformationen.value = Editer
[global]

# ---------------------------------------------
# Sitemap Element mit Klassen UL/LI abbilden
# In T3 (normale) "Sitemap" wählen und dann pro
# Hauptmenu den Oberordner.
# ---------------------------------------------
lib.sitemap = COA
lib.sitemap {
    stdWrap.prefixComment = 2 | lib.sitemap
    wrap = <div id="sitemap">|</div>
    10 = HMENU
    10 {
        special = directory
        # UID Hirarchie, welche mit subnavigationen angezeigt werden sollen (Bsp.: 11,45,78,...)
        # --------------------------------------------------------------------------------------
        special.value = 1
        # UID Hirarchie, welche NICHT angezeigt werden sollen (Bsp.: 11,45,78,...)
        # --------------------------------------------------------------------------------------
        excludeUidList = 9999
        1 = TMENU
        1 {
            wrap = <ul class="first">|</ul>
            noBlur = 1
            expAll = 1
            NO {
                wrapItemAndSub = <li class="next">|</li>|*|<li>|</li>|*|<li class="last">|</li>
                ATagTitle.field = title
                after {
                    # Anzeigen der META oder Abstract Infors nach dem Titel
                    # -----------------------------------------------------
                    # field = abstract // description
                    wrap = <br />|
                }
            }
        }

        2 < .1
        2 {
            wrap = <ul class="sub">|</ul>
        }

        3 < .2
        4 < .2
        5 < .2
    }
}

# Löschen aller alten Sitemap-Angaben
tt_content.menu.20.2 >
# Kopie der neuen Sitemap-Angaben
tt_content.menu.20.2 < lib.sitemap

# ------------ Content Einstellungen ----------
tt_content.stdWrap >
tt_content.image.20.maxW = 1500
tt_content.fluidcontent_content.10 >

# -----------------------------------
# Zusätzliche Metatags definieren
# -----------------------------------
page.meta {
    publisher = sesamnet GmbH
}

# -----------------------------------
# Jede Seite mit individueller ID versehen
# -----------------------------------
page.bodyTag >
page.bodyTagCObject = TEXT
page.bodyTagCObject.field = uid
page.bodyTagCObject.wrap = <body id="page-|">

# -----------------------------------
# CSS Style Sheet einbinden
# -----------------------------------
page = PAGE
page {
    includeJSlibs {
        #jQuery = {$resDir}Public/Scripts/jquery-1.11.3.min.js

    }

    includeJS {
        jquery = https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js
        jquery.external = 1
        jquery.forceOnTop = 1
        nicEdit = {$resDir}Public/Scripts/nicEdit.js
    }

    #Favicon einbinden
    shortcutIcon = favicon.ico

    #CSS
    includeCSS {
        master = EXT:snmetas/Resources/Public/Css/master.css
        master.media = screen

        printcss = EXT:snmetas/Resources/Public/Css/print.css
        printcss.media = print
    }

    # -----------------------------------------
    # Eigene Rahmen
    # ------------------------------------------
    #tt_content.stdWrap.innerWrap.cObject = CASE
    #tt_content.stdWrap.innerWrap.cObject {
    #  key.field = section_frame
    #  100 = TEXT
    #  100.value = <div class="sidebar">|</div>
    #}

    # -----------------------------------------
    # Layout-Felder Ueberschriften definieren
    # (Rest ausblenden)
    # ------------------------------------------

    lib.stdheader >
    lib.stdheader = CASE
    lib.stdheader {
        key.field = header_layout

        #H2
        1 = TEXT
        1.field = header
        1.wrap = <h2>|</h2>

        #H3
        2 = TEXT
        2.field = header
        2.wrap = <h3>|</h3>

        #H4
        3 = TEXT
        3.field = header
        3.wrap = <h4>|</h4>

        #H5
        4 = TEXT
        4.field = header
        4.wrap = <h5>|</h5>
    }

    # ENDE lib.stdheader

}