<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('snmetas', 'Configuration/TypoScript', 'SNMETAS');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('snmetas', 'Page');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('snmetas', 'Content');

