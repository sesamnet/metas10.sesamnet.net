<?php
namespace Sesamnet\Offiziellemitteilungen\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Gianluigi Martino <gianluigi.martino@sesamnet.ch>, sesamnet GmbH
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Sesamnet\Offiziellemitteilungen\Controller\OffizielleMitteilungController.
 *
 * @author Gianluigi Martino <gianluigi.martino@sesamnet.ch>
 */
class OffizielleMitteilungControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Sesamnet\Offiziellemitteilungen\Controller\OffizielleMitteilungController
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = $this->getMock('Sesamnet\\Offiziellemitteilungen\\Controller\\OffizielleMitteilungController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllOffizielleMitteilungsFromRepositoryAndAssignsThemToView() {

		$allOffizielleMitteilungs = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$offizielleMitteilungRepository = $this->getMock('Sesamnet\\Offiziellemitteilungen\\Domain\\Repository\\OffizielleMitteilungRepository', array('findAll'), array(), '', FALSE);
		$offizielleMitteilungRepository->expects($this->once())->method('findAll')->will($this->returnValue($allOffizielleMitteilungs));
		$this->inject($this->subject, 'offizielleMitteilungRepository', $offizielleMitteilungRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('offizielleMitteilungs', $allOffizielleMitteilungs);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function newActionAssignsTheGivenOffizielleMitteilungToView() {
		$offizielleMitteilung = new \Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('newOffizielleMitteilung', $offizielleMitteilung);
		$this->inject($this->subject, 'view', $view);

		$this->subject->newAction($offizielleMitteilung);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenOffizielleMitteilungToOffizielleMitteilungRepository() {
		$offizielleMitteilung = new \Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung();

		$offizielleMitteilungRepository = $this->getMock('Sesamnet\\Offiziellemitteilungen\\Domain\\Repository\\OffizielleMitteilungRepository', array('add'), array(), '', FALSE);
		$offizielleMitteilungRepository->expects($this->once())->method('add')->with($offizielleMitteilung);
		$this->inject($this->subject, 'offizielleMitteilungRepository', $offizielleMitteilungRepository);

		$this->subject->createAction($offizielleMitteilung);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenOffizielleMitteilungToView() {
		$offizielleMitteilung = new \Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('offizielleMitteilung', $offizielleMitteilung);

		$this->subject->editAction($offizielleMitteilung);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenOffizielleMitteilungInOffizielleMitteilungRepository() {
		$offizielleMitteilung = new \Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung();

		$offizielleMitteilungRepository = $this->getMock('Sesamnet\\Offiziellemitteilungen\\Domain\\Repository\\OffizielleMitteilungRepository', array('update'), array(), '', FALSE);
		$offizielleMitteilungRepository->expects($this->once())->method('update')->with($offizielleMitteilung);
		$this->inject($this->subject, 'offizielleMitteilungRepository', $offizielleMitteilungRepository);

		$this->subject->updateAction($offizielleMitteilung);
	}
}
