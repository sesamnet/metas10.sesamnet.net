<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_offiziellemitteilungen_domain_model_offiziellemitteilung', 'EXT:offiziellemitteilungen/Resources/Private/Language/locallang_csh_tx_offiziellemitteilungen_domain_model_offiziellemitteilung.xlf');
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_offiziellemitteilungen_domain_model_offiziellemitteilung');
