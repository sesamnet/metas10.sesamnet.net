<?php
namespace Sesamnet\Offiziellemitteilungen\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Gianluigi Martino <gianluigi.martino@sesamnet.ch>, sesamnet GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use Sesamnet\Offiziellemitteilungen\Property\TypeConverter\UploadedFileReferenceConverter;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration;


/**
 * OffizielleMitteilungController
 */
class OffizielleMitteilungController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * offizielleMitteilungRepository
	 *
	 * @var \Sesamnet\Offiziellemitteilungen\Domain\Repository\OffizielleMitteilungRepository
	 * @TYPO3\CMS\Extbase\Annotation\Inject
	 */
	protected $offizielleMitteilungRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$offizielleMitteilungs = $this->offizielleMitteilungRepository->findAll();


		foreach($offizielleMitteilungs as $mitteilung){
			$crdate = $mitteilung->getCrdate();
			$tstamp = $mitteilung->getTstamp();


			$endtime = $mitteilung->getEndtime();
			$starttime = $mitteilung->getStarttime();

			if ($starttime > 0) {
			    if ($endtime) {
                    if( time() > $starttime && time() < $endtime){
                        $returnMitteilung[] = $mitteilung;
                    }
                } else {
                    if( time() > $starttime && (strtotime('-3 days') < $crdate || strtotime('-3 days') < $tstamp) ){
                        $returnMitteilung[] = $mitteilung;
                    }
                }
            } else {
			    if ($endtime) {
                    if( time() < $endtime){
                        $returnMitteilung[] = $mitteilung;
                    }
                } else {
                    if( strtotime('-3 days') < $crdate || strtotime('-3 days') < $tstamp ){
                        $returnMitteilung[] = $mitteilung;
                    }
                }
            }
		}
		//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($postIts);
		//$this->view->assign('validDate', $validDate);
		//$this->view->assign('offizielleMitteilungs', $offizielleMitteilungs);

		$this->view->assign('offizielleMitteilungs', $returnMitteilung);

	}

	/**
	 * action new
	 */
	public function newAction() {
        $newOffizielleMitteilung = new \Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung();
	    $this->view->assign('newOffizielleMitteilung', $newOffizielleMitteilung);
	}
	
	/**
	 * Set TypeConverter option for image upload
	 */
	public function initializeCreateAction() {
		$this->setTypeConverterConfigurationForImageUpload('newOffizielleMitteilung');
	}

	/**
	 * action create
	 *
	 * @param \Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung $newOffizielleMitteilung
	 * @return void
	 */
	public function createAction(\Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung $newOffizielleMitteilung) {
		$this->offizielleMitteilungRepository->add($newOffizielleMitteilung);
        $this->addFlashMessage('Your new Mitteilung was created.');
        $this->redirect('list');
	}

	/**
	 * action edit
	 *
	 * @param \Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung $offizielleMitteilung
	 * @return void
	 */
	public function editAction(\Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung $offizielleMitteilung) {
		$this->view->assign('offizielleMitteilung', $offizielleMitteilung);
	}
	
	/**
	 * Set TypeConverter option for image upload
	 */
	public function initializeUpdateAction() {
		$this->setTypeConverterConfigurationForImageUpload('offizielleMitteilung');
	}

	/**
	 * action update
	 *
	 * @param \Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung $offizielleMitteilung
	 * @return void
	 */
	public function updateAction(\Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung $offizielleMitteilung) {
		$this->offizielleMitteilungRepository->update($offizielleMitteilung);
		$this->redirect('list');
	}
	
	/**
	 * action delete
	 *
	 * @param \Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung $offizielleMitteilung
	 * @return void
	 */
	public function deleteAction(\Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung $offizielleMitteilung) {
		$this->offizielleMitteilungRepository->remove($offizielleMitteilung);
		$this->redirect('list');
	}


	
	/**
	 *
	 */
	//protected function setTypeConverterConfigurationForImageUpload($argumentName) {
	//	$uploadConfiguration = array(
	//		UploadedFileReferenceConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
	//		UploadedFileReferenceConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/offiziellemeldungen/',
	//	);
	//	/** @var PropertyMappingConfiguration $newPostitConfiguration */
	//	$newPostitConfiguration = $this->arguments[$argumentName]->getPropertyMappingConfiguration();
	//	$newPostitConfiguration->forProperty('bild')
	//		->setTypeConverterOptions(
	//			'Sesamnet\\Offiziellemitteilungen\\Property\\TypeConverter\\UploadedFileReferenceConverter',
	//			$uploadConfiguration
	//		);
	//}


    /**
     *
     */
    protected function setTypeConverterConfigurationForImageUpload($argumentName)
    {
        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
            ->registerImplementation(
                \TYPO3\CMS\Extbase\Domain\Model\FileReference::class,
                \Sesamnet\Offiziellemitteilungen\Domain\Model\FileReference::class
            );

        $uploadConfiguration = [
            UploadedFileReferenceConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
            UploadedFileReferenceConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/content/',
        ];
        /** @var PropertyMappingConfiguration $newInteresseConfiguration */
        $newOffiziellemitteilungenConfiguration = $this->arguments[$argumentName]->getPropertyMappingConfiguration();
        $newOffiziellemitteilungenConfiguration->forProperty('bild.0')
            ->setTypeConverterOptions(
                UploadedFileReferenceConverter::class,
                $uploadConfiguration
            );
    }



}