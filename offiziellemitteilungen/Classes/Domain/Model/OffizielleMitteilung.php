<?php
namespace Sesamnet\Offiziellemitteilungen\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Gianluigi Martino <gianluigi.martino@sesamnet.ch>, sesamnet GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * OffizielleMitteilung
 */

use TYPO3\CMS\Extbase\Annotation\Validate;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;


class OffizielleMitteilung extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * titel
	 *
	 * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $titel = '';
	
	/** @var int */
	protected $crdate;

	/**
	* Returns the crdate
	*
	* @return int
	*/
	public function getCrdate() {
		return $this->crdate;
	}


    /** @var int */
    protected $endtime;

    /**
     * Returns the endtime
     *
     * @return int
     */
    public function getEndtime() {
        return $this->endtime;
    }


    /** @var int */
    protected $starttime;

    /**
     * Returns the starttime
     *
     * @return int
     */
    public function getStarttime() {
        return $this->starttime;
    }



    /** @var int */
	protected $tstamp;

	/**
	* Returns the tstamp
	*
	* @return int
	*/
	public function getTstamp() {
		return $this->tstamp;
	}

/**
	 * bild
	 *
	 * @var  \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 */
	protected $bild;

    public function __construct()
    {
        $this->bild = new ObjectStorage();
    }


    /**
	 * nachricht
	 *
	 * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $nachricht = '';

	/**
	 * Returns the titel
	 *
	 * @return string $titel
	 */
	public function getTitel() {
		return $this->titel;
	}

	/**
	 * Sets the titel
	 *
	 * @param string $titel
	 * @return void
	 */
	public function setTitel($titel) {
		$this->titel = $titel;
	}

    /**
     * Sets the bild
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $bild
     */
    public function setBild($bild) {
        $this->bild = $bild;
    }

    /**
     * Returns the bild
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getBild() {
        return $this->bild;
    }

	/**
	 * Returns the nachricht
	 *
	 * @return string $nachricht
	 */
	public function getNachricht() {
		return $this->nachricht;
	}

	/**
	 * Sets the nachricht
	 *
	 * @param string $nachricht
	 * @return void
	 */
	public function setNachricht($nachricht) {
		$this->nachricht = $nachricht;
	}

}