<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Offiziellemitteilungen',
    'Pi1',
    [
        \Sesamnet\Offiziellemitteilungen\Controller\OffizielleMitteilungController::class => 'list, show, new, create, edit, update, delete',
    ],
    // non-cacheable actions
    [
        \Sesamnet\Offiziellemitteilungen\Controller\OffizielleMitteilungController::class => 'list, show, new, create, edit, update, delete',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter('Sesamnet\\Offiziellemitteilungen\\Property\\TypeConverter\\UploadedFileReferenceConverter');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter('Sesamnet\\Offiziellemitteilungen\\Property\\TypeConverter\\ObjectStorageConverter');


