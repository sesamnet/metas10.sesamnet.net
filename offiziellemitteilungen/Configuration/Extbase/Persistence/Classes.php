<?php
declare(strict_types = 1);

return [
    \Sesamnet\Offiziellemitteilungen\Domain\Model\FileReference::class => [
        'tableName' => 'sys_file_reference',
        'properties' => [
            'uid_local' => [
                'fieldName' => 'originalFileIdentifier',
            ],
        ],
    ],
    \Sesamnet\Offiziellemitteilungen\Domain\Model\OffizielleMitteilung::class => [
        'tableName' => 'tx_offiziellemitteilungen_domain_model_offiziellemitteilung',
        'properties' => [
            'crdate' => [
                'fieldName' => 'crdate',
            ],
            'tstamp' => [
                'fieldName' => 'tstamp',
            ],
        ],
    ],
];
