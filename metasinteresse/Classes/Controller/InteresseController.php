<?php
namespace Sesamnet\Metasinteresse\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

use Sesamnet\Metasinteresse\Property\TypeConverter\UploadedFileReferenceConverter;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration;


/**
 * InteresseController
 */
class InteresseController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * interesseRepository
	 *
	 * @var \Sesamnet\Metasinteresse\Domain\Repository\InteresseRepository
	 * @TYPO3\CMS\Extbase\Annotation\Inject
	 */
	protected $interesseRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$interesses = $this->interesseRepository->findAll();

		/*
		foreach($interesses as $interesse){
			$crdate = $interesse->getCrdate();
			$tstamp = $interesse->getTstamp();
			if( strtotime('-3 days') < $crdate || strtotime('-3 days') < $tstamp ){
				$returnInteresse[] = $interesse;
			}
		}
		*/
		//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($interesses);
		//$this->view->assign('validDate', $validDate);
		//$this->view->assign('interesses', $interesses);
		$this->view->assign('interesses', $interesses);
	}

	/**
	 * action new
	 */
	public function newAction() {
	    $newInteresse = new \Sesamnet\Metasinteresse\Domain\Model\Interesse();
		$this->view->assign('newInteresse', $newInteresse);
	}


    /**
     * Set TypeConverter option for image upload
     */
    public function initializeCreateAction()
    {
        $this->setTypeConverterConfigurationForImageUpload('newInteresse');
    }

	/**
	 * action create
	 *
	 * @param \Sesamnet\Metasinteresse\Domain\Model\Interesse $newInteresse
	 * @return void
	 */
	public function createAction(\Sesamnet\Metasinteresse\Domain\Model\Interesse $newInteresse) {

 		$this->interesseRepository->add($newInteresse);
        $this->addFlashMessage('Your new Interesse was created.');
        $this->redirect('list');
 	}
 
	/**
	 * action edit
	 *
	 * @param \Sesamnet\Metasinteresse\Domain\Model\Interesse $interesse
	 */
	public function editAction(\Sesamnet\Metasinteresse\Domain\Model\Interesse $interesse) {
		$this->view->assign('interesse', $interesse);
	}



    /**
     * Set TypeConverter option for image upload
     */
    public function initializeUpdateAction()
    {
        $this->setTypeConverterConfigurationForImageUpload('interesse');
    }

	/**
	 * action update
	 *
	 * @param \Sesamnet\Metasinteresse\Domain\Model\Interesse $interesse
	 * @return void
	 */
	public function updateAction(\Sesamnet\Metasinteresse\Domain\Model\Interesse $interesse) {
		$this->interesseRepository->update($interesse);
        $this->addFlashMessage('Your new Interesse was updated.');
        $this->redirect('list');
	}


    /**
     *
     */
    protected function setTypeConverterConfigurationForImageUpload($argumentName)
    {
        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
            ->registerImplementation(
                \TYPO3\CMS\Extbase\Domain\Model\FileReference::class,
                \Sesamnet\Metasinteresse\Domain\Model\FileReference::class
            );

        $uploadConfiguration = [
            UploadedFileReferenceConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
            UploadedFileReferenceConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/interessantes/',
        ];
        /** @var PropertyMappingConfiguration $newInteresseConfiguration */
        $newInteresseConfiguration = $this->arguments[$argumentName]->getPropertyMappingConfiguration();
        $newInteresseConfiguration->forProperty('bild.0')
            ->setTypeConverterOptions(
                UploadedFileReferenceConverter::class,
                $uploadConfiguration
            );
    }

}