<?php
namespace Sesamnet\Metasinteresse\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Sesamnet\Metasinteresse\Controller\InteresseController.
 *
 */
class InteresseControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Sesamnet\Metasinteresse\Controller\InteresseController
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = $this->getMock('Sesamnet\\Metasinteresse\\Controller\\InteresseController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllInteressesFromRepositoryAndAssignsThemToView() {

		$allInteresses = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$interesseRepository = $this->getMock('Sesamnet\\Metasinteresse\\Domain\\Repository\\InteresseRepository', array('findAll'), array(), '', FALSE);
		$interesseRepository->expects($this->once())->method('findAll')->will($this->returnValue($allInteresses));
		$this->inject($this->subject, 'interesseRepository', $interesseRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('interesses', $allInteresses);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function newActionAssignsTheGivenInteresseToView() {
		$interesse = new \Sesamnet\Metasinteresse\Domain\Model\Interesse();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('newInteresse', $interesse);
		$this->inject($this->subject, 'view', $view);

		$this->subject->newAction($interesse);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenInteresseToInteresseRepository() {
		$interesse = new \Sesamnet\Metasinteresse\Domain\Model\Interesse();

		$interesseRepository = $this->getMock('Sesamnet\\Metasinteresse\\Domain\\Repository\\InteresseRepository', array('add'), array(), '', FALSE);
		$interesseRepository->expects($this->once())->method('add')->with($interesse);
		$this->inject($this->subject, 'interesseRepository', $interesseRepository);

		$this->subject->createAction($interesse);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenInteresseToView() {
		$interesse = new \Sesamnet\Metasinteresse\Domain\Model\Interesse();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('interesse', $interesse);

		$this->subject->editAction($interesse);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenInteresseInInteresseRepository() {
		$interesse = new \Sesamnet\Metasinteresse\Domain\Model\Interesse();

		$interesseRepository = $this->getMock('Sesamnet\\Metasinteresse\\Domain\\Repository\\InteresseRepository', array('update'), array(), '', FALSE);
		$interesseRepository->expects($this->once())->method('update')->with($interesse);
		$this->inject($this->subject, 'interesseRepository', $interesseRepository);

		$this->subject->updateAction($interesse);
	}
}
