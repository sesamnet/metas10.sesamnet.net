<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

/*
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Sesamnet.' . 'metasinteresse',
	'Metasinteresse',
	array(
		'Interesse' => 'list, new, create, edit, update',
		
	),
	// non-cacheable actions
	array(
		'Interesse' => 'list, new, create, edit, update',
		
	)
);
*/

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Metasinteresse',
    'Pi1',
    [
        \Sesamnet\Metasinteresse\Controller\InteresseController::class => 'list, show, new, create, edit, update, delete',
    ],
    // non-cacheable actions
    [
        \Sesamnet\Metasinteresse\Controller\InteresseController::class => 'list, show, new, create, edit, update, delete',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter('Sesamnet\\Metasinteresse\\Property\\TypeConverter\\UploadedFileReferenceConverter');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter('Sesamnet\\Metasinteresse\\Property\\TypeConverter\\ObjectStorageConverter');


