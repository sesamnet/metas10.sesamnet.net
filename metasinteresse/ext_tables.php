<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_metasinteresse_domain_model_interesse', 'EXT:metasinteresse/Resources/Private/Language/locallang_csh_tx_metasinteresse_domain_model_interesse.xlf');
