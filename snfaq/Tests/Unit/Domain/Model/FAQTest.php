<?php

namespace Sesamnet\Snfaq\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Jan Fässler <jan.faessler@sesamnet.ch>, sesamnet GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Sesamnet\Snfaq\Domain\Model\FAQ.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Jan Fässler <jan.faessler@sesamnet.ch>
 */
class FAQTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \Sesamnet\Snfaq\Domain\Model\FAQ
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \Sesamnet\Snfaq\Domain\Model\FAQ();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getFrageReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getFrage()
		);
	}

	/**
	 * @test
	 */
	public function setFrageForStringSetsFrage() {
		$this->subject->setFrage('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'frage',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAntwortReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getAntwort()
		);
	}

	/**
	 * @test
	 */
	public function setAntwortForStringSetsAntwort() {
		$this->subject->setAntwort('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'antwort',
			$this->subject
		);
	}
}
