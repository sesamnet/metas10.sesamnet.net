<?php
namespace Sesamnet\Snfaq\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Jan Fässler <jan.faessler@sesamnet.ch>, sesamnet GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Annotation\Validate;
/**
 * FAQ
 */
class FAQ extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * frage
	 *
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $frage = '';

	/**
	 * antwort
	 *
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $antwort = '';

	/**
	 * Returns the frage
	 *
	 * @return string $frage
	 */
	public function getFrage() {
		return $this->frage;
	}

	/**
	 * Sets the frage
	 *
	 * @param string $frage
	 * @return void
	 */
	public function setFrage($frage) {
		$this->frage = $frage;
	}

	/**
	 * Returns the antwort
	 *
	 * @return string $antwort
	 */
	public function getAntwort() {
		return $this->antwort;
	}

	/**
	 * Sets the antwort
	 *
	 * @param string $antwort
	 * @return void
	 */
	public function setAntwort($antwort) {
		$this->antwort = $antwort;
	}

}