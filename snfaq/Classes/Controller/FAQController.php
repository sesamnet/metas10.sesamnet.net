<?php
namespace Sesamnet\Snfaq\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Jan Fässler <jan.faessler@sesamnet.ch>, sesamnet GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * FAQController
 */
use TYPO3\CMS\Extbase\Annotation\Inject;
use TYPO3\CMS\Extbase\Annotation\IgnoreValidation;

class FAQController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * fAQRepository
	 *
	 * @var \Sesamnet\Snfaq\Domain\Repository\FAQRepository
	 * @TYPO3\CMS\Extbase\Annotation\Inject
	 */
	protected $fAQRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$fAQs = $this->fAQRepository->findAll();
		$this->view->assign('fAQs', $fAQs);
	}

}