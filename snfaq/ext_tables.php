<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sesamnet.' . 'snfaq',
    'Faq',
	'FAQ'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('snfaq', 'Configuration/TypoScript', 'SN FAQ');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_snfaq_domain_model_faq', 'EXT:snfaq/Resources/Private/Language/locallang_csh_tx_snfaq_domain_model_faq.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_snfaq_domain_model_faq');

/*
$GLOBALS['TCA']['tx_snfaq_domain_model_faq'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:snfaq/Resources/Private/Language/locallang_db.xlf:tx_snfaq_domain_model_faq',
		'label' => 'frage',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'frage,antwort,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('snfaq') . 'Configuration/TCA/FAQ.php',
		'iconfile' => 'EXT:snfaq/Resources/Public/Icons/tx_snfaq_domain_model_faq.gif'
	),
);
*/