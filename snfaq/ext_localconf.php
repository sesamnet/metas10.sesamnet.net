<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Sesamnet.' . 'snfaq',
	'Faq',
	array(
		'FAQ' => 'list',
		
	),
	// non-cacheable actions
	array(
		'FAQ' => '',
		
	)
);
