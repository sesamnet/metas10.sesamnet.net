<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_metaspostit_domain_model_postit', 'EXT:metaspostit/Resources/Private/Language/locallang_csh_tx_metaspostit_domain_model_postit.xlf');
