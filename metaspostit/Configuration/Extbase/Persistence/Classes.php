<?php
declare(strict_types = 1);

return [
    \Sesamnet\Metaspostit\Domain\Model\FileReference::class => [
        'tableName' => 'sys_file_reference',
        'properties' => [
            'uid_local' => [
                'fieldName' => 'originalFileIdentifier',
            ],
        ],
    ],
    \Sesamnet\Metaspostit\Domain\Model\PostIt::class => [
        'tableName' => 'tx_metaspostit_domain_model_postit',
        'properties' => [
            'crdate' => [
                'fieldName' => 'crdate',
            ],
            'tstamp' => [
                'fieldName' => 'tstamp',
            ],
        ],
    ],
];
