<?php
namespace Sesamnet\Metaspostit\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

use Sesamnet\Metaspostit\Property\TypeConverter\UploadedFileReferenceConverter;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration;


/**
 * PostItController
 */
class PostItController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * postItRepository
	 *
	 * @var \Sesamnet\Metaspostit\Domain\Repository\PostItRepository
	 * @TYPO3\CMS\Extbase\Annotation\Inject
	 */
	protected $postItRepository = NULL;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$postIts = $this->postItRepository->findAll();


		foreach($postIts as $postit){
			$crdate = $postit->getCrdate();
			$tstamp = $postit->getTstamp();

            if( strtotime('-3 days') < $crdate || strtotime('-3 days') < $tstamp ){
				$returnPostit[] = $postit;
			}
		}


		//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($postIts);
		//$this->view->assign('validDate', $validDate);
		//$this->view->assign('postIts', $postIts);
		$this->view->assign('postIts', $returnPostit);
	}

	/**
	 * action new
	 */
	public function newAction() {
	    $newPostIt = new \Sesamnet\Metaspostit\Domain\Model\PostIt();
		$this->view->assign('newPostIt', $newPostIt);
	}


    /**
     * Set TypeConverter option for image upload
     */
    public function initializeCreateAction()
    {
        $this->setTypeConverterConfigurationForImageUpload('newPostIt');
    }

	/**
	 * action create
	 *
	 * @param \Sesamnet\Metaspostit\Domain\Model\PostIt $newPostIt
	 * @return void
	 */
	public function createAction(\Sesamnet\Metaspostit\Domain\Model\PostIt $newPostIt) {

 		$this->postItRepository->add($newPostIt);
        $this->addFlashMessage('Your new Postit was created.');
        $this->redirect('list');
 	}
 
	/**
	 * action edit
	 *
	 * @param \Sesamnet\Metaspostit\Domain\Model\PostIt $postIt
	 */
	public function editAction(\Sesamnet\Metaspostit\Domain\Model\PostIt $postIt) {
		$this->view->assign('postIt', $postIt);
	}



    /**
     * Set TypeConverter option for image upload
     */
    public function initializeUpdateAction()
    {
        $this->setTypeConverterConfigurationForImageUpload('postIt');
    }

	/**
	 * action update
	 *
	 * @param \Sesamnet\Metaspostit\Domain\Model\PostIt $postIt
	 * @return void
	 */
	public function updateAction(\Sesamnet\Metaspostit\Domain\Model\PostIt $postIt) {
		$this->postItRepository->update($postIt);
        $this->addFlashMessage('Your new Postit was updated.');
        $this->redirect('list');
	}


    /**
     *
     */
    protected function setTypeConverterConfigurationForImageUpload($argumentName)
    {
        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
            ->registerImplementation(
                \TYPO3\CMS\Extbase\Domain\Model\FileReference::class,
                \Sesamnet\Metaspostit\Domain\Model\FileReference::class
            );

        $uploadConfiguration = [
            UploadedFileReferenceConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
            UploadedFileReferenceConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/postit/',
        ];
        /** @var PropertyMappingConfiguration $newPostItConfiguration */
        $newPostItConfiguration = $this->arguments[$argumentName]->getPropertyMappingConfiguration();
        $newPostItConfiguration->forProperty('bild.0')
            ->setTypeConverterOptions(
                UploadedFileReferenceConverter::class,
                $uploadConfiguration
            );
    }

}