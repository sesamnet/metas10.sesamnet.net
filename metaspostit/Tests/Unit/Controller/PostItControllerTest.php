<?php
namespace Sesamnet\Metaspostit\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Sesamnet\Metaspostit\Controller\PostItController.
 *
 */
class PostItControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Sesamnet\Metaspostit\Controller\PostItController
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = $this->getMock('Sesamnet\\Metaspostit\\Controller\\PostItController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllPostItsFromRepositoryAndAssignsThemToView() {

		$allPostIts = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$postItRepository = $this->getMock('Sesamnet\\Metaspostit\\Domain\\Repository\\PostItRepository', array('findAll'), array(), '', FALSE);
		$postItRepository->expects($this->once())->method('findAll')->will($this->returnValue($allPostIts));
		$this->inject($this->subject, 'postItRepository', $postItRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('postIts', $allPostIts);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function newActionAssignsTheGivenPostItToView() {
		$postIt = new \Sesamnet\Metaspostit\Domain\Model\PostIt();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('newPostIt', $postIt);
		$this->inject($this->subject, 'view', $view);

		$this->subject->newAction($postIt);
	}

	/**
	 * @test
	 */
	public function createActionAddsTheGivenPostItToPostItRepository() {
		$postIt = new \Sesamnet\Metaspostit\Domain\Model\PostIt();

		$postItRepository = $this->getMock('Sesamnet\\Metaspostit\\Domain\\Repository\\PostItRepository', array('add'), array(), '', FALSE);
		$postItRepository->expects($this->once())->method('add')->with($postIt);
		$this->inject($this->subject, 'postItRepository', $postItRepository);

		$this->subject->createAction($postIt);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenPostItToView() {
		$postIt = new \Sesamnet\Metaspostit\Domain\Model\PostIt();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('postIt', $postIt);

		$this->subject->editAction($postIt);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenPostItInPostItRepository() {
		$postIt = new \Sesamnet\Metaspostit\Domain\Model\PostIt();

		$postItRepository = $this->getMock('Sesamnet\\Metaspostit\\Domain\\Repository\\PostItRepository', array('update'), array(), '', FALSE);
		$postItRepository->expects($this->once())->method('update')->with($postIt);
		$this->inject($this->subject, 'postItRepository', $postItRepository);

		$this->subject->updateAction($postIt);
	}
}
