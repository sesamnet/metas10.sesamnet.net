<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

/*
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Sesamnet.' . 'metaspostit',
	'Metaspostit',
	array(
		'PostIt' => 'list, new, create, edit, update',
		
	),
	// non-cacheable actions
	array(
		'PostIt' => 'list, new, create, edit, update',
		
	)
);
*/

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Metaspostit',
    'Pi1',
    [
        \Sesamnet\Metaspostit\Controller\PostItController::class => 'list, show, new, create, edit, update, delete',
    ],
    // non-cacheable actions
    [
        \Sesamnet\Metaspostit\Controller\PostItController::class => 'list, show, new, create, edit, update, delete',
    ]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter('Sesamnet\\Metaspostit\\Property\\TypeConverter\\UploadedFileReferenceConverter');
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter('Sesamnet\\Metaspostit\\Property\\TypeConverter\\ObjectStorageConverter');


