<?php

// plugin signature: <extension key without underscores> '_' <plugin name in lowercase>
$pluginSignature = 'snquicklinks_anzeigequicklinks';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    // Flexform configuration schema file
    'FILE:EXT:snquicklinks/Configuration/FlexForms/flexform_quicklinks.xml'
);