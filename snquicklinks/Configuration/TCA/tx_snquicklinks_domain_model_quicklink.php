<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

//$GLOBALS['TCA']['tx_snquicklinks_domain_model_quicklink'];

return array(
    'ctrl' => array(
        'title'	=> 'LLL:EXT:snquicklinks/Resources/Private/Language/locallang_db.xlf:tx_snquicklinks_domain_model_quicklink',
        'label' => 'titel',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => TRUE,
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'searchFields' => 'titel,link,',
        'iconfile' => 'EXT:snquicklinks/Resources/Public/Icons/tx_snquicklinks_domain_model_quicklink.gif'
    ),
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, titel, link, categories',
    ),
    'types' => array(
        '1' => array('showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, titel, link, categories, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
    ),
    'palettes' => array(
        '1' => array('showitem' => ''),
    ),
    'columns' => array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_snquicklinks_domain_model_quicklink',
                'foreign_table_where' => 'AND tx_snquicklinks_domain_model_quicklink.pid=###CURRENT_PID### AND tx_snquicklinks_domain_model_quicklink.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),

        't3ver_label' => array(
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            )
        ),


        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'titel' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:snquicklinks/Resources/Private/Language/locallang_db.xlf:tx_snquicklinks_domain_model_quicklink.titel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ),
        ),

        'link' => [
            'label' => 'Link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
            ],
        ],

        /*
        'link' => array(
            'exclude' => 0,
            'label' => 'Link',
            'config' => array(
               'type' => 'input',
               'size' => 30,
               'max' => 512,
               'eval' => 'trim',
               'wizards' => array(
                   'link' => array(
                       'type' => 'popup',
                       'title' => 'LLL:EXT:cms/locallang_ttc.xlf:header_link_formlabel',
                       'icon' => '../typo3/sysext/backend/Resources/Public/Images/FormFieldWizard/wizard_link.gif',
                       'module' => array(
                           'name' => 'wizard_element_browser',
                           'urlParameters' => array(
                               'mode' => 'wizard'
                           )
                       ),
                       'JSopenParams' => 'height=300,width=1000,status=0,menubar=0,scrollbars=1'
                   )
               ),
               'softref' => 'typolink'
            ),
        ),
        */
    ),
);
