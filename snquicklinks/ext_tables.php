<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sesamnet.' . 'snquicklinks',
    'Anzeigequicklinks',
	'Anzeige Quicklinks'
);


// flexform
$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase('snquicklinks');
$pluginSignature = strtolower($extensionName) . '_anzeigequicklinks'; 
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'.'snquicklinks'.'/Configuration/FlexForms/flexform_quicklinks.xml');


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('snquicklinks', 'Configuration/TypoScript', 'SNQuicklinks');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_snquicklinks_domain_model_quicklink', 'EXT:snquicklinks/Resources/Private/Language/locallang_csh_tx_snquicklinks_domain_model_quicklink.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_snquicklinks_domain_model_quicklink');

/*
$GLOBALS['TCA']['tx_snquicklinks_domain_model_quicklink'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:snquicklinks/Resources/Private/Language/locallang_db.xlf:tx_snquicklinks_domain_model_quicklink',
		'label' => 'titel',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'titel,link,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('snquicklinks') . 'Configuration/TCA/tx_snquicklinks_domain_model_quicklink.php',
		'iconfile' => 'EXT:snquicklinks/Resources/Public/Icons/tx_snquicklinks_domain_model_quicklink.gif'
	),
);
*/