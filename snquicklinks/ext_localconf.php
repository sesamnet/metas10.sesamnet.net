<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Sesamnet.' . 'snquicklinks',
	'Anzeigequicklinks',
	array(
		'Quicklink' => 'list',
		
	),
	// non-cacheable actions
	array(
		'Quicklink' => 'list',		
	)
);
