<?php
namespace Sesamnet\Snquicklinks\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Gianluigi Martino <gianluigi.martino@sesamnet.ch>, sesamnet GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * QuicklinkController
 */
use TYPO3\CMS\Extbase\Annotation\Inject;
use TYPO3\CMS\Extbase\Annotation\IgnoreValidation;

class QuicklinkController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * quicklinkRepository
	 *
	 * @var \Sesamnet\Snquicklinks\Domain\Repository\QuicklinkRepository
	 * @TYPO3\CMS\Extbase\Annotation\Inject
	 */
	protected $quicklinkRepository;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		//$quicklinks = $this->quicklinkRepository->findAll();
		//var_dump($this->settings['flexform']['quicklinkitems']);
		//exit();
		$quicklinkslist = $this->settings['flexform']['quicklinkitems'];
		$quicklinks = $this->quicklinkRepository->findByUids($quicklinkslist);
		$this->view->assign('quicklinks', $quicklinks);
	}
}